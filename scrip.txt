
TABLAS A CREAR:



create table CARGOS(
idCargo serial primary key,
nombreCargo varchar(180)

);

create table USUARIOS( 
idUsuario serial primary key,
nombreUsuario varchar(180), 
edad integer not null,
idCargo integer not null,
fechaIngreso varchar(180),
FOREIGN KEY(idCargo) REFERENCES cargos (idCargo)
);

create table PRODUCTOS( 
idProducto serial primary key,
nombreProducto varchar(180), 
fechaIngreso varchar(180),
cantidad integer not null,
idUsuarioModifica integer not null,
idUsuarioRegistra  integer not null,
fechaModifica varchar(180)

);




create sequence sec_idproducto
  start with 1
  increment by 1
  maxvalue 99999
  minvalue 1;


create sequence sec_idcargo
  start with 1
  increment by 1
  maxvalue 99999
  minvalue 1;


create sequence sec_idusuario
  start with 1
  increment by 1
  maxvalue 99999
  minvalue 1;




