package co.com.nexos.utilities;

public class MsjExcepcion extends Exception {
	
	private static final long serialVersionUID = 1L;

	public MsjExcepcion(String msn) {
		super(msn);
	}

}
