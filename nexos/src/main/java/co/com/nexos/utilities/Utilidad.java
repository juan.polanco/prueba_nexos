package co.com.nexos.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Service
public class Utilidad {

	private static final Logger LOGGER = LogManager.getLogger(Utilidad.class);
	private static final String ERROR = "Error es:";
	
	public void cerrarConexion (Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) {
		
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.info(ERROR, e);
			}
		}
		
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				LOGGER.info(ERROR, e);
			}
		}
		
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.info(ERROR, e);
			}
		}
	}
	
}
