package co.com.nexos.utilities;

public enum EnumConsultas {
	
	VALIDACION_CONEXION("SELECT * FROM USUARIOS"),
	ELIMINAR_PRODUCTO("DELETE FROM PRODUCTOS WHERE idProducto = ?"),
	CONSULTAR_PRODUCTO_ID("SELECT idProducto,nombreProducto, fechaIngreso,cantidad,idUsuarioModifica,idUsuarioRegistra,fechaModifica FROM PRODUCTOS WHERE idProducto = ?"),
	CONSULTAR_PRODUCTOS("SELECT idProducto,nombreProducto, fechaIngreso,cantidad,idUsuarioModifica,idUsuarioRegistra,fechaModifica FROM PRODUCTOS"),
	CREAR_PRODUCTO("INSERT INTO PRODUCTOS(idproducto,nombreProducto,fechaIngreso,cantidad,idusuariomodifica,idUsuarioRegistra,fechamodifica)\n" + 
			"VALUES (nextval('productos_idproducto_seq'),?,?,?,?,?,?)"),
	ACTUALIZAR_PRODUCTO("UPDATE PRODUCTOS SET nombreProducto = ? ,cantidad = ? ,idUsuarioModifica =?,fechaModifica=?  WHERE idProducto = ?"),
	CREAR_CARGO("INSERT INTO CARGOS(idCargo,nombreCargo) VALUES (nextval('cargos_idcargo_seq'), ? )"),
	CONSULTAR_USUARIO("SELECT idUsuario,nombreUsuario,edad,idCargo,fechaIngreso FROM USUARIOS"),
	CREAR_USUARIO("INSERT INTO USUARIOS (idUsuario,nombreUsuario,edad,idCargo,fechaIngreso) VALUES (nextval('usuarios_idusuario_seq'),?,?,?,?)"),
	CONSULTAR_CARGOS("SELECT idCargo,nombreCargo FROM CARGOS");
	
private String consulta;
	
	
	private EnumConsultas(String consulta) {
		this.consulta = consulta;
	}


	public String getConsulta() {
		return consulta;
	}


	void setConsulta(String consulta) {
		this.consulta = consulta;
	}

}
