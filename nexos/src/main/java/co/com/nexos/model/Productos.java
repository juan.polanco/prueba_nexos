package co.com.nexos.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "Productos")
public class Productos {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idproducto;

	
	private String nombre; 
	private String fechaingreso;
	private int cantidad;
	private int idusuariomod;
	private int idusuarioreg;
	private String fechamod;
	
	public Productos(String nombre, String fechaingreso, int cantidad, int idusuariomod,
			int idusuarioreg,String fechamod) {
		super();
		this.nombre = nombre;
		this.fechaingreso = fechaingreso;
		this.cantidad = cantidad;
		this.idusuariomod = idusuariomod;
		this.idusuarioreg = idusuarioreg;
		this.fechamod = fechamod;
	}
	
	public Productos() {}

	public Long getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(Long idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(String fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getIdusuariomod() {
		return idusuariomod;
	}

	public void setIdusuariomod(int idusuariomod) {
		this.idusuariomod = idusuariomod;
	}

	public int getIdusuarioreg() {
		return idusuarioreg;
	}

	public void setIdusuarioreg(int idusuarioreg) {
		this.idusuarioreg = idusuarioreg;
	}

	public String getFechamod() {
		return fechamod;
	}

	public void setFechamod(String fechamod) {
		this.fechamod = fechamod;
	}

}
