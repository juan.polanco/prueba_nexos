package co.com.nexos.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "Cargos")
public class Cargos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idcargo;	
	
	private String nombre;
	
	public Cargos() {
	}
	
	
	public Cargos(String nombre) {
		super();
		this.nombre = nombre;
	}


	public Long getIdcargo() {
		return idcargo;
	}


	public void setIdcargo(Long idcargo) {
		this.idcargo = idcargo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
	
}
