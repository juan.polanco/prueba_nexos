package co.com.nexos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import co.com.nexos.dao.ProductosDao;
import co.com.nexos.dto.ProductosDto;
import co.com.nexos.utilities.MsjExcepcion;

@Service
public class ProductoServices   {
	
	@Autowired
	private ProductosDao productosDao;

	private Gson gson = new Gson();

	
	
	public List<ProductosDto> consultarProductos() throws MsjExcepcion {
		List<ProductosDto> strRespuesta = null;
			strRespuesta = productosDao.consultarProducto();
		return strRespuesta;
	}
	
	
	public String crearProductos(String strProducto) throws MsjExcepcion {
		
		ProductosDto objPersonaDto = gson.fromJson(strProducto, ProductosDto.class);
		

		String strRespuesta = productosDao.crearProducto(objPersonaDto);
		
		if (strRespuesta == null) {
			strRespuesta = "Producto creado";
		}
		
		return strRespuesta;
		
	}
	
	
	public String actualizarProductos(String strProducto) throws MsjExcepcion {
		
		ProductosDto objPersonaDto = gson.fromJson(strProducto, ProductosDto.class);
		

		String strRespuesta = productosDao.actualizarProducto(objPersonaDto);
		
	
		
		return strRespuesta;
		
	}
	
	
	public String eliminarProducto(ProductosDto productosDto) throws MsjExcepcion {
		
		List<ProductosDto> objProductoDto = null;
		

		
		String strRespuesta = null;		
			
		objProductoDto = productosDao.consultarProductoId(productosDto.getIdProducto());
		
		if (objProductoDto.isEmpty()) {
			
			throw new MsjExcepcion("No existe producto");
			
		} 
		
		strRespuesta = productosDao.eliminarProducto(productosDto.getIdProducto());
		
		
		
		
		return strRespuesta;
	}

}
