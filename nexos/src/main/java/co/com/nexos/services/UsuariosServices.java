package co.com.nexos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import co.com.nexos.dao.UsuarioDao;
import co.com.nexos.dto.UsuarioDto;
import co.com.nexos.utilities.MsjExcepcion;

@Service
public class UsuariosServices {
	
	
	@Autowired
	private UsuarioDao usuarioDao;
	private Gson gson = new Gson();
	
	public List<UsuarioDto> consultarUsuarios() throws MsjExcepcion {
		List<UsuarioDto> strRespuesta = null;
			strRespuesta = usuarioDao.consultarUsuarios();
		return strRespuesta;
	}
	
	
	
	
	public String crearUsuario(String strUsuario) throws MsjExcepcion {
		
		
		UsuarioDto usuarioDto = gson.fromJson(strUsuario, UsuarioDto.class);
		
				
		
		String strRespuesta = usuarioDao.crearUsuario(usuarioDto);
		
		
		
		return strRespuesta;
		
	}
	
	

}
