package co.com.nexos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.nexos.dao.CargoDao;
import co.com.nexos.dto.CargoDto;
import co.com.nexos.utilities.MsjExcepcion;

@Service
public class CargosServices {
	
	@Autowired
	private CargoDao cargoDao;

	
	public List<CargoDto> consultarCargos() throws MsjExcepcion {
		List<CargoDto> strRespuesta = null;
			strRespuesta = cargoDao.consultarCargos();
		return strRespuesta;
	}
	
	
	
	
	public String crearCargo(CargoDto strCargo) throws MsjExcepcion {			
		
		String strRespuesta = cargoDao.crearCargo(strCargo);

		return strRespuesta;
		
	}
	
	
	

}
