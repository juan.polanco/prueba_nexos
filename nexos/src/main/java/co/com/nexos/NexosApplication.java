package co.com.nexos;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import co.com.nexos.configuration.ConfiguracionDb;
import co.com.nexos.configuration.SqlJDBC;
import co.com.nexos.utilities.EnumConsultas;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
@Configuration
public class NexosApplication {

	private static final Logger LOGGER = LogManager.getLogger(NexosApplication.class);
	private static ApplicationContext ctx;

	
	@Autowired
	private ConfiguracionDb configuracionDb;
	@Autowired
	private SqlJDBC sqlJDBC;
	
	// Configuracion base de datos

	public static String usuario;
	public static String clave;
	
	@PostConstruct
	public void cargarVariables() {
		setUsuario(configuracionDb.getConfiguracionesBaseDatos().getUsuario());
		setClave(configuracionDb.getConfiguracionesBaseDatos().getClave());
		
		this.validarConexionBD();

		/** new ProcesarImpresion().leeImprime();*/
	}

	
	public static void main(String[] args) {
		SpringApplication.run(NexosApplication.class, args);
	}
	
	public void validarConexionBD() {
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conexion = sqlJDBC.getConexion();

			st = conexion.createStatement();
			rs = st.executeQuery(EnumConsultas.VALIDACION_CONEXION.getConsulta());
			
			
			
			while (rs.next()) {
				String strLog = String.format("\n***\n***id[%s] Conexion a base de datos EXITOSA \n***\n", Thread.currentThread().getId());
				LOGGER.info(strLog);
			}

		} catch (Exception e) {			
			String strLog = String.format("\n***\n***id[%s] Error obteniendo conexion a base de datos error es: %s \n***\n", Thread.currentThread().getId(), e.toString());
			LOGGER.info(strLog);
			
			String strLog2 = String.format("\n***\n***id[%s] Deteniendo inicializacion de microservicio: %s \n***\n", Thread.currentThread().getId(), e.toString());
			LOGGER.info(strLog2);
			
			try {
				if(ctx!=null) {
					((FileOutputStream) ctx).close();
				}
			} catch (IOException e1) {
				LOGGER.error(e1.getMessage());
			}
		} finally {
			if (conexion != null) {
				try {
					conexion.close();
				} catch (Exception e) {
					LOGGER.error(e.getMessage());
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (Exception e2) {
					LOGGER.error(e2.getMessage());
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e2) {
					LOGGER.error(e2.getMessage());
				}
			}
		}
	}
	
	

	public static String getUsuario() {
		return usuario;
	}

	public static void setUsuario(String usuario) {
		NexosApplication.usuario = usuario;
	}

	public static String getClave() {
		return clave;
	}

	public static void setClave(String clave) {
		NexosApplication.clave = clave;
	}
	
	

}
