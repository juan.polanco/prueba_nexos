package co.com.nexos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.com.nexos.configuration.SqlJDBC;
import co.com.nexos.dto.CargoDto;
import co.com.nexos.utilities.EnumConsultas;
import co.com.nexos.utilities.Utilidad;

@Scope("prototype")
@Repository
public class CargoDao {
	
	@Autowired
	private SqlJDBC sqlJDBC;
	
	@Autowired 
	private Utilidad utilidad;
	
	
	public List<CargoDto> consultarCargos() {
		
		List<CargoDto> lstCargos = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CONSULTAR_CARGOS.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);

			resultSet = preparedStatement.executeQuery();	
			
			while (resultSet.next()) {
				CargoDto objCargo= new CargoDto();
				objCargo.setIdCargo(resultSet.getInt("idCargo"));
				objCargo.setNombreCargo(resultSet.getString("nombreCargo"));
				
				
				
				lstCargos.add(objCargo);
				
			}
			
			
		} catch (Exception e) {
			e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return lstCargos;
		
	}
	
	
	
	public String crearCargo(CargoDto objCargo) {
		
		String respuesta = null;		
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CREAR_CARGO.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, objCargo.getNombreCargo());
			preparedStatement.executeUpdate();		
			
			respuesta = "cargo creada";
			
			
		} catch (Exception e) {
			respuesta = e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return respuesta;
		
	}

}
