package co.com.nexos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.com.nexos.configuration.SqlJDBC;
import co.com.nexos.dto.ProductosDto;
import co.com.nexos.utilities.EnumConsultas;
import co.com.nexos.utilities.Utilidad;

@Scope("prototype")
@Repository
public class ProductosDao {
	
	@Autowired
	private SqlJDBC sqlJDBC;
	
	@Autowired 
	private Utilidad utilidad;
	
	public List<ProductosDto> consultarProducto() {
		
		List<ProductosDto> lstProductos = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CONSULTAR_PRODUCTOS.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);

			resultSet = preparedStatement.executeQuery();	
			
			while (resultSet.next()) {
				ProductosDto objProducto= new ProductosDto();
				objProducto.setIdProducto(resultSet.getInt("idProducto"));
				objProducto.setNombreProducto(resultSet.getString("nombreProducto"));
				objProducto.setFechaIngreso(resultSet.getString("fechaIngreso"));
				objProducto.setCantidad(resultSet.getInt("cantidad"));
				objProducto.setIdUsuarioModifica(resultSet.getInt("idUsuarioModifica"));
				objProducto.setIdUsuarioRegistra(resultSet.getInt("idUsuarioRegistra"));
				objProducto.setFechaModifica(resultSet.getString("fechaModifica"));
				
				
				lstProductos.add(objProducto);
				
			}
			
			
		} catch (Exception e) {
			e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return lstProductos;
		
	}
	
	
	public String crearProducto(ProductosDto objProductoDto) {
		
		String respuesta = null;		
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CREAR_PRODUCTO.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, objProductoDto.getNombreProducto());
			preparedStatement.setString(2, objProductoDto.getFechaIngreso());
			preparedStatement.setInt(3, objProductoDto.getCantidad());
			preparedStatement.setInt(4, objProductoDto.getIdUsuarioModifica());
			preparedStatement.setInt(5, objProductoDto.getIdUsuarioRegistra());
			preparedStatement.setString(6, objProductoDto.getFechaIngreso());
	
			
			
			preparedStatement.executeUpdate();			
			
		} catch (Exception e) {
			respuesta = e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return respuesta;
		
	}
	
	
	public List<ProductosDto> consultarProductoId(int strId) {
		
		List<ProductosDto> lstProductos = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CONSULTAR_PRODUCTO_ID.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, strId);
			resultSet = preparedStatement.executeQuery();	
			
			while (resultSet.next()) {
				ProductosDto objProducto= new ProductosDto();

				objProducto.setIdProducto(resultSet.getInt("idProducto"));
				objProducto.setNombreProducto(resultSet.getString("nombreProducto"));
				objProducto.setFechaIngreso(resultSet.getString("fechaIngreso"));
				objProducto.setCantidad(resultSet.getInt("cantidad"));
				objProducto.setIdUsuarioModifica(resultSet.getInt("idUsuarioModifica"));
				objProducto.setIdUsuarioRegistra(resultSet.getInt("idUsuarioRegistra"));
				objProducto.setFechaModifica(resultSet.getString("fechaModifica"));
				
				lstProductos.add(objProducto);
			}		
			
		} catch (Exception e) {
			e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return lstProductos;
		
	}

	
	public String eliminarProducto(int strId) {	
		
		String strRespuesta = null;
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.ELIMINAR_PRODUCTO.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, strId);
			preparedStatement.executeUpdate();	
			
			strRespuesta = "Producto eliminado";
		}catch (SQLIntegrityConstraintViolationException sqle){
			strRespuesta = "No se puede eliminar producto.";
		}
		catch (Exception e) {
			strRespuesta = e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return strRespuesta;
		
	}
	
	
	
	
		public String actualizarProducto(ProductosDto objProductoDto) {
		
		String respuesta = null;		
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.ACTUALIZAR_PRODUCTO.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, objProductoDto.getNombreProducto());
			preparedStatement.setInt(2, objProductoDto.getCantidad());
			preparedStatement.setInt(3, objProductoDto.getIdUsuarioModifica());
			preparedStatement.setString(4, objProductoDto.getFechaModifica());
			preparedStatement.setInt(5, objProductoDto.getIdProducto());

			
			
			preparedStatement.executeUpdate();	
			respuesta = "Producto actualizado";
			
		} catch (Exception e) {
			respuesta = e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return respuesta;
		
	}

}
