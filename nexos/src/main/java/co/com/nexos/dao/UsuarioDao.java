package co.com.nexos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.com.nexos.configuration.SqlJDBC;
import co.com.nexos.dto.UsuarioDto;
import co.com.nexos.utilities.EnumConsultas;
import co.com.nexos.utilities.Utilidad;

@Scope("prototype")
@Repository
public class UsuarioDao {
	
	@Autowired
	private SqlJDBC sqlJDBC;
	
	@Autowired 
	private Utilidad utilidad;
	
	
	public List<UsuarioDto> consultarUsuarios() {
		
		List<UsuarioDto> lstCargos = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CONSULTAR_USUARIO.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);

			resultSet = preparedStatement.executeQuery();	
			
			while (resultSet.next()) {
				UsuarioDto objUsuario= new UsuarioDto();
				objUsuario.setIdUsuario(resultSet.getInt("idUsuario"));
				objUsuario.setNombreUsuario(resultSet.getString("nombreUsuario"));
				objUsuario.setEdad(resultSet.getInt("edad"));
				objUsuario.setIdCargo(resultSet.getInt("idCargo"));
				objUsuario.setFechaIngreso(resultSet.getString("fechaIngreso")); 

				lstCargos.add(objUsuario);
				
			}
			
			
		} catch (Exception e) {
			e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return lstCargos;
		
	}
	
	
	
	public String crearUsuario(UsuarioDto objUsuario) {
		
		String respuesta = null;		
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			
			String sql = EnumConsultas.CREAR_USUARIO.getConsulta();
			connection = sqlJDBC.getConexion();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, objUsuario.getNombreUsuario());
			preparedStatement.setInt(2, objUsuario.getEdad());
			preparedStatement.setInt(3, objUsuario.getIdCargo());
			preparedStatement.setString(4, objUsuario.getFechaIngreso());

			
			preparedStatement.executeUpdate();	
			
			respuesta = "usuarios creado";
			
			
		} catch (Exception e) {
			respuesta = e.getMessage();
		} finally {
			utilidad.cerrarConexion(connection, preparedStatement, resultSet);
		}		
		
		return respuesta;
		
	}

}
