package co.com.nexos.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="configuraciones")
public class ConfiguracionDb {
	
	private ConfiguracionesBaseDatos configuracionesBaseDatos = new ConfiguracionesBaseDatos();	
	
	public ConfiguracionesBaseDatos getConfiguracionesBaseDatos() {
		return configuracionesBaseDatos;
	}
	public void setConfiguracionesBaseDatos(ConfiguracionesBaseDatos configuracionesBaseDatos) {
		this.configuracionesBaseDatos = configuracionesBaseDatos;
	}


	public class ConfiguracionesBaseDatos {
		
		private String dataBase;
		private String hostName;
		private String puerto;
		private String usuario;
		private String clave;
		
		
		
		public String getDataBase() {
			return dataBase;
		}
		public void setDataBase(String dataBase) {
			this.dataBase = dataBase;
		}
		public String getHostName() {
			return hostName;
		}
		public void setHostName(String hostName) {
			this.hostName = hostName;
		}
		public String getPuerto() {
			return puerto;
		}
		public void setPuerto(String puerto) {
			this.puerto = puerto;
		}
		public String getUsuario() {
			return usuario;
		}
		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		public String getClave() {
			return clave;
		}
		public void setClave(String clave) {
			this.clave = clave;
		} 
		
	}
}
