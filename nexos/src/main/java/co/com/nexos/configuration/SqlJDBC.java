package co.com.nexos.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


import co.com.nexos.NexosApplication;

@Service
@Scope("prototype")
public class SqlJDBC {
	
	private static final String DRIVER = "org.postgresql.Driver";
	
	public Connection getConexion() throws SQLException, ClassNotFoundException {
		
		StringBuilder str = new StringBuilder();		
		str.append("jdbc:postgresql://localhost/postgres?user=");
		str.append(NexosApplication.usuario);
		str.append("&password=");
		str.append(NexosApplication.clave);
		str.append("&ssl=false");

		final String URL = str.toString();

		Connection conexion = null;

		Class.forName(DRIVER);
		conexion = DriverManager.getConnection(URL);
		
		return conexion;
	}

}
