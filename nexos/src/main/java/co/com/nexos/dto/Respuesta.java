package co.com.nexos.dto;

public class Respuesta<T> {

	private String strEstadoRetorno;
	private String strMensaje;
	private T retorno;
	
	public String getStrEstadoRetorno() {
		return strEstadoRetorno;
	}
	public void setStrEstadoRetorno(String strEstadoRetorno) {
		this.strEstadoRetorno = strEstadoRetorno;
	}
	public String getStrMensaje() {
		return strMensaje;
	}
	public void setStrMensaje(String strMensaje) {
		this.strMensaje = strMensaje;
	}
	public T getRetorno() {
		return retorno;
	}
	public void setRetorno(T retorno) {
		this.retorno = retorno;
	}	
	
}
