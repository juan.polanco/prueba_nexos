package co.com.nexos.dto;

public class ProductosDto {
	
	
private int idProducto;
private String nombreProducto;
private String fechaIngreso;
private int cantidad;
private int idUsuarioModifica;
private int idUsuarioRegistra;
private String fechaModifica;


public int getIdProducto() {
	return idProducto;
}
public void setIdProducto(int idProducto) {
	this.idProducto = idProducto;
}
public String getNombreProducto() {
	return nombreProducto;
}
public void setNombreProducto(String nombreProducto) {
	this.nombreProducto = nombreProducto;
}
public String getFechaIngreso() {
	return fechaIngreso;
}
public void setFechaIngreso(String fechaIngreso) {
	this.fechaIngreso = fechaIngreso;
}
public int getCantidad() {
	return cantidad;
}
public void setCantidad(int cantidad) {
	this.cantidad = cantidad;
}
public int getIdUsuarioModifica() {
	return idUsuarioModifica;
}
public void setIdUsuarioModifica(int idUsuarioModifica) {
	this.idUsuarioModifica = idUsuarioModifica;
}
public int getIdUsuarioRegistra() {
	return idUsuarioRegistra;
}
public void setIdUsuarioRegistra(int idUsuarioRegistra) {
	this.idUsuarioRegistra = idUsuarioRegistra;
}
public String getFechaModifica() {
	return fechaModifica;
}
public void setFechaModifica(String fechaModifica) {
	this.fechaModifica = fechaModifica;
}
	

	
	

}
