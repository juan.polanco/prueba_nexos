package co.com.nexos.rest;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.nexos.dto.ProductosDto;
import co.com.nexos.dto.Respuesta;
import co.com.nexos.services.ProductoServices;

@CrossOrigin
@RestController
public class ProductoRest {
	
	
	@Autowired
	private ProductoServices productoServices;

	@PostMapping(value="/ConsultaProductos", produces=MediaType.APPLICATION_JSON)
	public Respuesta<List<ProductosDto>> consultarTipoPersonas() {

		Respuesta<List<ProductosDto>> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(productoServices.consultarProductos());
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}
	
	@PostMapping(value="/CrearProducto", produces=MediaType.APPLICATION_JSON)
	public Respuesta<String> crearProducto(@RequestBody String peticion) {

		Respuesta<String> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(productoServices.crearProductos(peticion));
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}
	
	

	@PostMapping(value="/eliminarProducto", produces=MediaType.APPLICATION_JSON)
	private Respuesta<String> eliminarProducto (@RequestBody ProductosDto productosDto){

		Respuesta<String> objRes = new Respuesta<>();
		try {
			objRes.setRetorno(productoServices.eliminarProducto(productosDto));
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		
		return objRes;
		
	}
	
	@PostMapping(value="/ActualizarProducto", produces=MediaType.APPLICATION_JSON)
	public Respuesta<String> actualizarProducto(@RequestBody String peticion) {

		Respuesta<String> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(productoServices.actualizarProductos(peticion));
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}
	
	
	

}
