package co.com.nexos.rest;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.nexos.dto.CargoDto;
import co.com.nexos.dto.Respuesta;
import co.com.nexos.services.CargosServices;

@CrossOrigin
@RestController
public class CargosRest {
	
	@Autowired
	private CargosServices cargosServices;
	
	

	@PostMapping(value="/ConsultaCargos", produces=MediaType.APPLICATION_JSON)
	public Respuesta<List<CargoDto>> consultarCargos() {

		Respuesta<List<CargoDto>> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(cargosServices.consultarCargos());
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}
	
	@PostMapping(value="/CrearCargo", produces=MediaType.APPLICATION_JSON)
	public Respuesta<String> crearProducto(@RequestBody CargoDto cargoDto) {

		Respuesta<String> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(cargosServices.crearCargo(cargoDto));
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}
	
	
	

	
	

}
