package co.com.nexos.rest;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.nexos.dto.Respuesta;
import co.com.nexos.dto.UsuarioDto;
import co.com.nexos.services.UsuariosServices;

@CrossOrigin
@RestController
public class UsuariosRest {
	
	@Autowired
	private UsuariosServices usuariosServices;
	
	@PostMapping(value="/ConsultarUsuarios", produces=MediaType.APPLICATION_JSON)
	public Respuesta<List<UsuarioDto>> consultarUsuarios() {

		Respuesta<List<UsuarioDto>> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(usuariosServices.consultarUsuarios());
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}
	
	@PostMapping(value="/CrearUsuario", produces=MediaType.APPLICATION_JSON)
	public Respuesta<String> crearProducto(@RequestBody String peticion) {

		Respuesta<String> objRes = new Respuesta<>();
		
		try {
			objRes.setRetorno(usuariosServices.crearUsuario(peticion));
		} catch (Exception e) {
			objRes.setStrMensaje(e.getMessage());
		}
		return objRes;
	}

}
